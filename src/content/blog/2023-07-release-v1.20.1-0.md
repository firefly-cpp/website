---
title: Forgejo v1.20 is available
publishDate: 2023-07-24
tags: ['releases']
release: 1.20.1-0
excerpt: Forgejo v1.20 is available and comes with an integrated CI, still in alpha stage but robust enough to build this release and verify all Forgejo pull requests. This release also brings customizable user profiles, a simpler Markdown editor, user blocking for self moderation, pinned issues, six more registries and many new API endpoints. As always, make sure to carefully read the breaking changes from the release notes and make a full backup before upgrading.
---

[Forgejo v1.20.1-0](https://forgejo.org/download/) is here and you will find the most interesting changes it introduces below. Before upgrading it is **strongly recommended** to make a full backup as explained in the [upgrade guide](https://forgejo.org/docs/v1.20/admin/upgrade/) and carefully read **all breaking changes** from the [release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#1-20-1-0). If in doubt, do not hesitate to ask for help [on the Fediverse](https://floss.social/@forgejo),
or in the [chat room](https://matrix.to/#/#forgejo-chat:matrix.org).

- **[Actions](/docs/v1.20/user/actions)**: the internal CI graduated from **experimental** to **[alpha](https://en.wikipedia.org/wiki/Software_release_life_cycle#Alpha)** and is now used by Forgejo to verify pull requests and to create releases, including this one. It comes with a [user](/docs/v1.20/user/actions) documentation that includes examples and an extensive [administrator](/docs/v1.20/admin/actions) guide to set it up.
  ![List of actions](./_images/2023-07-actions-step1.png)
- **User profile**: the Forgejo home page for a user [can now be a Markdown file](https://codeberg.org/forgejo/forgejo/commit/c090f87a8db5b51e0aa9c7278b38ddc862c048ac) instead of the list of repositories they own.
  ![Profile page](../docs/v1.20/_images/user/profile/profile-step1.png)
- **New markdown editor**: the editor used when creating issues, adding comments, etc. is now [GitHub markdown](https://github.com/github/markdown-toolbar-element).
  ![GitHub markdown editor](./_images/2023-07-markdown-step2.png)
- **[Blocking users](/docs/v1.20/user/blocking-user/):** is a new self-moderation tool a user or an organization can use to prevent users from interacting with the repositories they own.
  ![Forbidden interaction from the point of view of the blocked user](./_images/2023-07-block-step4.png)
- **[Pinned issues](https://codeberg.org/forgejo/forgejo/commit/aaa109466350c531b9238a61115b2877daca57d3)**: it is now possible to select issues and pull requests to show on top of the list.
  ![Pinned issues](./_images/2023-07-pin-step1.png)
- **Registries:** additional registries are now available for [SWIFT](https://forgejo.org/docs/v1.20/user/packages/swift), [debian](https://forgejo.org/docs/v1.20/user/packages/debian), [RPM](https://forgejo.org/docs/v1.20/user/packages/rpm), [alpine](https://forgejo.org/docs/v1.20/user/packages/alpine), [Go](https://forgejo.org/docs/v1.20/user/packages/go) and [CRAN](https://forgejo.org/docs/v1.20/user/packages/cran).
- **API endpoints:** new API endpoints are now available for [email](https://codeberg.org/forgejo/forgejo/commit/d56bb7420184c0c2f451f4bcaa96c9b3b00c393d), [renaming a user](https://codeberg.org/forgejo/forgejo/commit/03591f0f95823a0b1dcca969d2a3ed505c7e6d73), [issue dependencies management](https://codeberg.org/forgejo/forgejo/commit/3cab9c6b0c050bfcb9f2f067e7dc1b0242875254), [activity feeds](https://codeberg.org/forgejo/forgejo/commit/6b0df6d8da76d77a9b5c42dcfa78dbfe197fd56d), [license templates](https://codeberg.org/forgejo/forgejo/commit/fb37eefa282543fd8ce63c361cd4cf0dfac9943c), [gitignore templates](https://codeberg.org/forgejo/forgejo/commit/36a5d4c2f3b5670e5e921034cd5d25817534a6d4), [uploading files to an empty repository](https://codeberg.org/forgejo/forgejo/commit/cf465b472166ccf6d3e001e3043e4bf43e16e6b3), [creating a branch directly from commit](https://codeberg.org/forgejo/forgejo/commit/cd9a13ebb47d32f46b38439a524e3b2e0c619490), [label templates](https://codeberg.org/forgejo/forgejo/commit/25dc1556cd70b567a4920beb002a0addfbfd6ef2), [changing/creating/deleting multiple files](https://codeberg.org/forgejo/forgejo/commit/275d4b7e3f4595206e5c4b1657d4f6d6969d9ce2).

Read more [in the Forgejo v1.20.1-0 release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#1-20-1-0).

### Forgejo Actions

Although [Forgejo Actions](/docs/v1.20/user/actions) is **not yet production ready**, it became good enough for Forgejo itself to use in production. It [verifies pull requests](https://codeberg.org/forgejo/forgejo/actions) (see also the [testing workflow](https://codeberg.org/forgejo/forgejo/src/commit/60b10cd66051ffd4cbdbae9a4aa63aa0f55b2e8d/.forgejo/workflows/testing.yml)), [builds](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/.forgejo/workflows/build-release.yml) and [publishes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/.forgejo/workflows/publish-release.yml) releases (this one and the release candidates before it).

![List of actions](./_images/2023-07-actions-step1.png)

It is still considered **[alpha](https://en.wikipedia.org/wiki/Software_release_life_cycle#Alpha)** stage because:

- the [Forgejo runner](https://code.forgejo.org/forgejo/runner) is **not secure enough**
- a single `Forgejo runner` will poll `Forgejo` every two seconds by default which is not scalable
- some errors only show in the `Forgejo runner` logs and not in the `Forgejo` user interface which is not a good user experience

![Details of a task](./_images/2023-07-actions-step2.png)

The potential security bugs are a concern and `Forgejo` took the following precautions to reduce the risks in its own infrastructure.

- **Do not trust any web application with secrets.** The `Forgejo`
  release process needs a GPG private key to sign the binaries
  before they are
  [uploaded](https://codeberg.org/forgejo/forgejo/releases). A web
  application with a large attack surface such as `Forgejo` or
  `GitLab` must not be trusted to keep such a secret safe. Instead a
  `Forgejo` instance dedicated to signing the releases was installed
  behind a VPN.
- **LXC containers confinement.** All `Forgejo runners` are deployed
  in [dedicated LXC
  containers](/docs/v1.20/developer/infrastructure/#installing-forgejo-runners)
  and re-installed from scratch from time to time.

In addition, the [required pull request
approval](/docs/v1.20/user/actions/#pull-request-actions-are-moderated)
prevents unknown users from triggering a task that would include a
malicious workflow.

### User profile

By default the profile page of a user is the list of repositories they
own. It is possible to customize it with a short description that
shows to the left, under their avatar. It can now be fully
personalized with a markdown file that is displayed instead of the
list of repositories.

![Profile page](../docs/v1.20/_images/user/profile/profile-step1.png)

[Read more user profile customization](/docs/v1.20/user/profile/).

### New markdown editor

The web editor used when creating issues, adding comments,
etc. [changed](https://codeberg.org/forgejo/forgejo/commit/5cc0801de90d16b4d528e62de11c9b525be5d122)
from [EasyMDE](https://github.com/Ionaru/easy-markdown-editor) to
[GitHub
markdown](https://github.com/github/markdown-toolbar-element). To help
with the transition it is still possible to switch back to using
EasyMDE with the double arrow button in the menubar.

![GitHub markdown editor](./_images/2023-07-markdown-step2.png)

This new markdown editor does not provide any WYSIWIG features. As
[shown in the
demo](https://github.github.com/markdown-toolbar-element/examples/) it
is merely a helper for users who are not familiar with
[markdown](https://en.wikipedia.org/wiki/Markdown).

![GitHub markdown example](./_images/2023-07-markdown-step3.png)

Want to add a list? Click on the list menu item and see
that a star is inserted for you. Select a word and click the bold
button so it is surrounded by two stars. Nothing fancier. By
comparison the
[EasyMDE](https://github.com/Ionaru/easy-markdown-editor) editor has
more features such as showing in bold the word that is surrounded by two stars.

![EasyMDE editor](./_images/2023-07-markdown-step1.png)

Unfortunately it is no longer actively maintained and enough has long
standing bugs to justify a replacement.

### Blocking users

On large Forgejo instances with ten of thousands of users it may be
challenging for the moderation team to properly address all
requests. The most common one being a malicious user spamming issues
with advertisements or unwanted noise. It will be immediately noticed
by the repository owner and it may take a while for the moderation
team to act.

The owner of a repository or an organization can now block a user as
soon as they notice an undesirable interaction. When they go to the
profile page of the user, a new **Block** button shows on the left.

![Block button on user profile](./_images/2023-07-block-step1.png)

After confirmation the user will be added to the list of blocked users.

![Blocking a user confirmation](./_images/2023-07-block-step2.png)

From the **Blocked Users** tab in their profile, the user can unblock
them when the relationship gets better.

![List of blocked users](./_images/2023-07-block-step3.png)

The user being blocked is not notified and does not see any difference
until they try to participate in a repository from which they are
blocked. Their action will fail with a message informing them they
have been blocked.

![Forbidden interaction from the point of view of the blocked user](./_images/2023-07-block-step4.png)

[Read more about blocking users](/docs/v1.20/user/blocking-user/).

### Pin issues

[Issues and pull requests can be pinned](https://codeberg.org/forgejo/forgejo/commit/aaa109466350c531b9238a61115b2877daca57d3) and will show on top of the list of issues (or pull requests). They can be re-arranged by dragging them.

![Profile page](./_images/2023-07-pin-step1.png)

### Theming and custom templates

The themes and templates changed a lot in this release and there is no
documentation explaining how and why. The hope is that the users will
discover the changes and not be overly confused.

This is also a reminder that Forgejo considers themes and templates to
be a part of the internals and require an understanding of the source
codebase to be modified and adapted after each release. In other
words, if a Forgejo admin extracted templates and modified them on a
v1.19 instance they will need to read the source code to figure out
how they need to be modified to keep working with v1.20.

### Federation

Does `Forgejo` support federation? Not yet. Was there progress? Yes.

The monthly report [has details](https://forgejo.org/tag/report/) on
these progress and the [State of the Forge Federation: 2023
edition](https://forgefriends.org/blog/2023/06/21/2023-06-state-forge-federation/)
published last month explains how Forgejo fits in the big picture.

Forges have existed for twenty years and none of them has achieved
data portability let alone federation. Forgejo is yet to celebrate its
first birthday and it will take it a little time to get there. One
thing is for sure: at this point no other forge is doing concrete work
in this direction.

### Get Forgejo v1.20

See the [download page](/download)
for instructions on how to install Forgejo, and read the
[release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#1-20-1-0)
for more information.

### Upgrading

Carefully read
[the breaking changes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#1-20-1-0)
section of the release notes.

The actual upgrade process is as simple as replacing the binary or container image
with the corresponding [Forgejo binary](https://codeberg.org/forgejo/forgejo/releases/tag/v1.20.1-0)
or [container image](https://codeberg.org/forgejo/-/packages/container/forgejo/1.20.1-0).
If you're using the container images, you can use the
[`1.20` tag](https://codeberg.org/forgejo/-/packages/container/forgejo/1.20)
to stay up to date with the latest `1.20.x` point release automatically.

Make sure to check the [Forgejo upgrade
documentation](/docs/v1.20/admin/upgrade) for
recommendations on how to properly backup your instance before the
upgrade. It also covers upgrading from Gitea, as far back as version 1.2.0.
Forgejo includes all of Gitea v1.20.

### Contribute to Forgejo

If you have any feedback or suggestions for Forgejo do not hold back, it is also your project.
Open an issue in [the issue tracker](https://codeberg.org/forgejo/forgejo/issues)
for feature requests or bug reports, reach out [on the Fediverse](https://floss.social/@forgejo),
or drop into [the Matrix space](https://matrix.to/#/#forgejo:matrix.org)
([main chat room](https://matrix.to/#/#forgejo-chat:matrix.org)) and say hi!
