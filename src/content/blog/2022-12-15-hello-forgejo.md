---
title: Beyond coding. We forge.
publishDate: 2022-12-15
tags: ['news']
excerpt: After many days of hard work and preparation, we are proud to announce that the Forgejo project is now live.
---

After many days of hard work and preparation by a team of former Gitea maintainers and enthusiasts from the FOSS community, we are proud to announce that the Forgejo project is now live.

**Forgejo** ([/forˈd͡ʒe.jo/](/static/forgejo.mp4) – inspired by <i lang="eo">forĝejo</i>, the Esperanto word for forge) is a community-driven Free Software project that develops a code forge platform similar to GitHub, and that is a drop-in replacement for Gitea. We started Forgejo in reaction to control of Gitea being taken away from the community by the newly-formed for-profit company Gitea Ltd without prior community consultation, and after an [Open Letter](https://gitea-open-letter.coding.social/) to the Gitea project owners remained unanswered.
The Forgejo project has two major objectives that drive our development and road map:

1. The community is in control, and ensures we develop to address community needs.
2. We will help liberate software development from the shackles of proprietary tools.

### We are people in control of our future

The first objective relates to how we are organized as a project. It is crucial for Forgejo to guarantee that our product will remain Free Software forever, under the guidance of an open and inclusive community. Forgejo will provide a healthy project governance, so that it can truly focus on the needs of all those people that use our software on a daily basis.

To this end we are very proud that Codeberg e.V. has decided to become our project’s custodian. Codeberg e.V. is a non-profit organization with a stellar reputation, that is dedicated to the success of the Free Software movement. They provide software development services to FOSS projects at Codeberg.org. They are rapidly growing and hosting more than 50,000 code repositories for about 40,000 people. Not only will Codeberg take care of the Forgejo domain names and trademarks, but the organization will use Forgejo as the basis for their own services, instead of Gitea.

Forgejo's code base is of course hosted on Codeberg, and by using Woodpecker CI instead of Drone and Matrix instead of Discord, we exclusively rely on Free Software tools.

### We will help liberate software development

Our second objective relates to the product that we deliver. Free Software projects are in general heavily focused on coding. But development of quality software also involves work in many other areas. For the long-term sustainability of any Free Software, project many aspects must be taken into account. Successful software products involve collaboration between many people with different skill sets.

Forgejo's vision is to make software development accessible to everyone – bringing true inclusion and diversity to a field traditionally dominated by technical-skilled people, and where many of the tools used are proprietary services with companies dictating who has access, and who does not.

Our vision offers our community a very exciting path forward. For our development road map we will consider the entirety of the Free Software Development Lifecycle (FSDL) and gradually unlock it to the world. We intend to be part of a growing ecosystem of collaborating Free Software projects that dedicate to this same vision. We see [forge federation](https://forgefriends.org/blog/2022/06/30/2022-06-state-forge-federation/) as a means to open up the development process, and it is here that we will innovate.

### Join our adventure

Help make coding social. Join us in our quest, be part of our adventure. Forgejo is not just a code forge. We go beyond coding. **Together we forge Free Software!**

_Stay tuned to our blog. The coming period we will provide you with many updates about our project organization._
