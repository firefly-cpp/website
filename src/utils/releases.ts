import { getCollection, type CollectionEntry } from 'astro:content';

import { giteaApi, type Release } from 'gitea-js';
const api = giteaApi('https://codeberg.org', {});

let _releases: Release[];
let _releaseBlogPosts: CollectionEntry<'blog'>[];

/** Fetch a list of the latest Forgejo releases from Codeberg using the Forgejo API. */
export const fetchReleases = async () => {
	if (!_releases) {
		const response = await api.repos.repoListReleases('forgejo', 'forgejo');
		_releases = response.data;
	}
	return _releases;
};

/** Get the latest Forgejo release from Codeberg using the Forgejo API. */
export const getLatestRelease = async () => {
	const releases = await fetchReleases();
	return releases[0];
};

/** Get the blog post for a release, if there is one. */
export const getReleaseBlogPost = async (version: string) => {
	if (!_releaseBlogPosts) {
		_releaseBlogPosts = await getCollection('blog', ({ data }) => data.release);
	}
	return _releaseBlogPosts.find(({ data }) => data.release == version);
};
